# To change this license header, choose License Headers in Project Properties.
# To change this template file, choose Tools | Templates
# and open the template in the editor.

__author__="Beto"
__date__ ="$24/06/2014 08:43:04 PM$"

from bs4 import BeautifulSoup
import urllib2

url = "http://ocw.mit.edu/courses/physics/8-04-quantum-physics-i-spring-2013/calendar/"
header = {'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; rv:17.0) Gecko/17.0 Firefox/17.0'}
req = urllib2.Request(url,headers=header)

content = urllib2.urlopen(req)
soup = BeautifulSoup(content)

clase = ""
tema = ""
palabrasClave = ""
resultado = ""

info = soup.find("table", { "class" : "tablewidth75" })
contador = 0
for row in info.findAll("tr"):
    row.string
    cells = row.findAll("td")
  
    if len(cells) == 3:
        clase = cells[0].find(text=True).encode('utf-8')
        tema = cells[1].find(text=True).encode('utf-8')      
        palabrasClave = cells[2].find(text=True).encode('utf-8') 
        
        resultado  = clase+", "+tema+", "+palabrasClave+"."
        
        print resultado